### Install using releases

#### Introduction

At Commento, we aim to have a release on the first Wednesday of each month. Every fifth release point is marked long-term support (LTS), meaning security patches will be backported for up to 10 months. As a result, LTS releases will not receive new features (unless absolutely critical). If you strongly prefer stability, it is recommended you use the latest LTS version. If you prefer new features and performance improvements, you're advised to use the latest stable release.

CE and EE follow the same release cycle.

#### Release binaries

At the moment, we do not have release binaries, but we plan on offering them shortly. For now, you can download the source for each release and [install from source](installation-source.md).
